document
  .getElementById("sort-btn")
  .addEventListener("click", sortNumberFromSmallToLarge);

function sortNumberFromSmallToLarge() {
  // Input var
  var firstNumValue = document.getElementById("1stNum-input").value * 1;
  var secondNumValue = document.getElementById("2ndNum-input").value * 1;
  var thirdNumValue = document.getElementById("3rdNum-input").value * 1;
  var fourthNumValue = document.getElementById("4thNum-input").value * 1;
  var fifthNumValue = document.getElementById("5thNum-input").value * 1;
  // Output var
  var sortFirstThreeNumberOutput = document.getElementById(
    "sort-first-three-number-output"
  );
  var sortAllFiveNumberOutput = document.getElementById(
    "sort-all-five-number-output"
  );
  var equal = " = ";
  var smaller = " < ";

  // Sort for first three number
  if (firstNumValue <= secondNumValue) {
    if (firstNumValue == secondNumValue) {
      if (firstNumValue < thirdNumValue) {
        sortFirstThreeNumberOutput.innerHTML =
          firstNumValue + equal + secondNumValue + smaller + thirdNumValue;
      } else if (firstNumValue > thirdNumValue) {
        sortFirstThreeNumberOutput.innerHTML =
          thirdNumValue + smaller + firstNumValue + equal + secondNumValue;
      } else {
        sortFirstThreeNumberOutput.innerHTML =
          firstNumValue + equal + secondNumValue + equal + thirdNumValue;
      }
    } else {
      if (secondNumValue <= thirdNumValue) {
        if (secondNumValue < thirdNumValue) {
          sortFirstThreeNumberOutput.innerHTML =
            firstNumValue + smaller + secondNumValue + smaller + thirdNumValue;
        } else {
          sortFirstThreeNumberOutput.innerHTML =
            firstNumValue + smaller + secondNumValue + equal + thirdNumValue;
        }
      } else {
        if (firstNumValue <= thirdNumValue) {
          if (firstNumValue < thirdNumValue) {
            sortFirstThreeNumberOutput.innerHTML =
              firstNumValue +
              smaller +
              thirdNumValue +
              smaller +
              secondNumValue;
          } else {
            sortFirstThreeNumberOutput.innerHTML =
              firstNumValue + equal + thirdNumValue + smaller + secondNumValue;
          }
        } else {
          sortFirstThreeNumberOutput.innerHTML =
            thirdNumValue + smaller + firstNumValue + smaller + secondNumValue;
        }
      }
    }
  } else {
    if (secondNumValue >= thirdNumValue) {
      if (secondNumValue > thirdNumValue) {
        sortFirstThreeNumberOutput.innerHTML =
          thirdNumValue + smaller + secondNumValue + smaller + firstNumValue;
      } else {
        sortFirstThreeNumberOutput.innerHTML =
          secondNumValue + equal + thirdNumValue + smaller + firstNumValue;
      }
    } else {
      if (firstNumValue <= thirdNumValue) {
        if (firstNumValue < thirdNumValue) {
          sortFirstThreeNumberOutput.innerHTML =
            secondNumValue + smaller + firstNumValue + smaller + thirdNumValue;
        } else {
          sortFirstThreeNumberOutput.innerHTML =
            secondNumValue + smaller + firstNumValue + equal + thirdNumValue;
        }
      } else {
        sortFirstThreeNumberOutput.innerHTML =
          secondNumValue + smaller + thirdNumValue + smaller + firstNumValue;
      }
    }
  }

  // Sort for all five number
  // Tạo mảng cho đầu vào
  var numberInputArray = [
    firstNumValue,
    secondNumValue,
    thirdNumValue,
    fourthNumValue,
    fifthNumValue,
  ];
  // Tạo mảng cho vị trí sắp xếp của số
  var sortOder = [0, 0, 0, 0, 0];

  // Tạo vòng lặp thứ nhất để lấy số cần mang đi so sánh
  for (i = 0; i < numberInputArray.length; ++i) {
    // Tạo vòng lặp thứ hai để so sánh lần lượt với các số khác trong mảng
    for (j = 0; j < numberInputArray.length; ++j) {
      // Nếu số đó nhỏ hơn hoặc bằng số được so sánh, vị trí đứng giữ nguyên
      if (numberInputArray[i] <= numberInputArray[j]) {
        sortOder[i] = sortOder[i];
      } // Ngược lại, vị trí đứng cộng một tức là lùi về sau
      else {
        sortOder[i] = sortOder[i] + 1;
      }
    }
  }

  // Tạo mảng để in ra kết quả đã sắp xếp
  var numberOutputArray = [];
  numberOutputArray[sortOder[0]] = numberInputArray[0];
  numberOutputArray[sortOder[1]] = numberInputArray[1];
  numberOutputArray[sortOder[2]] = numberInputArray[2];
  numberOutputArray[sortOder[3]] = numberInputArray[3];
  numberOutputArray[sortOder[4]] = numberInputArray[4];

  console.log(numberInputArray);
  console.log(sortOder);
  console.log(numberOutputArray);

  // Tạo vòng lặp để giải quyết trường hợp có hai số bằng nhau
  for (k = 0; k < numberOutputArray.length; ++k) {
    if (numberOutputArray[k] === undefined) {
      numberOutputArray[k] = numberOutputArray[k - 1];
    }
  }
  sortAllFiveNumberOutput.innerHTML = numberOutputArray;
}
