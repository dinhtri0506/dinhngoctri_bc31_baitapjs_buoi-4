document
  .getElementById("btn-checkHowManyEvenAndOddNumbers")
  .addEventListener("click", checkHowManyEvenAndOddNumbers);

function checkHowManyEvenAndOddNumbers() {
  // Input
  // Kiểm tra phần số dư khi chia cho 2 (bằng 0 là chẵn, khác 0 là lẻ)
  // Số chẵn là số nguyên chia hết cho 2, nên dùng parseInt để lấy số nguyên (không có cũng không sao)
  var divisionRemainderOfFirstNum =
    parseInt(document.getElementById("firstNumToCheck").value * 1) % 2;
  var divisionRemainderOfSecondNum =
    parseInt(document.getElementById("secondNumToCheck").value * 1) % 2;
  var divisionRemainderOfThirdNum =
    parseInt(document.getElementById("thirdNumToCheck").value * 1) % 2;
  // Tạo một mảng để sử dụng vòng lặp
  var checkedArray = [
    divisionRemainderOfFirstNum,
    divisionRemainderOfSecondNum,
    divisionRemainderOfThirdNum,
  ];

  var numberOfEven = 0;
  var numberOfOdd = 0;

  for (i = 0; i < checkedArray.length; i++) {
    switch (checkedArray[i]) {
      // Kiểm tra nếu số dư bằng không, biến số lượng của số chẵn cộng thêm 1
      case 0:
        numberOfEven += 1;
        break;
      // Ngược lại, biến số lượng số lẻ cộng thêm 1
      default:
        numberOfOdd += 1;
    }
  }

  // Output
  console.log({ numberOfEven }, { numberOfOdd });
  document.getElementById(
    "checkedOutput"
  ).innerHTML = `Số lượng số chẵn = ${numberOfEven} <br />
Số lượng số lẻ = ${numberOfOdd}`;
}
