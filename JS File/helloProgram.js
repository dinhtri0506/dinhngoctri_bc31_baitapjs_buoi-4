document.getElementById("btn-hello").addEventListener("click", sayHello);

document.getElementById("members").addEventListener("click", sayHello);

function sayHello() {
  // Input
  // Tạo biến để xác định giá trị user đang chọn
  // Ép kiểu dữ liệu để so sánh dễ hơn
  var memberValue = document.getElementById("members").value * 1;
  var member;

  // Dùng switch case để có thể cho ra kết quả phù hợp với từng trường hợp
  switch (memberValue) {
    case 0:
      member = "bố";
      break;
    case 1:
      member = "mẹ";
      break;
    case 2:
      member = "anh trai";
      break;
    case 3:
      member = "em gái";
      break;
  }

  // Output
  console.log(memberValue);
  document.getElementById(
    "helloProgramOutput"
  ).innerHTML = `Xin chào ${member}`;
}
