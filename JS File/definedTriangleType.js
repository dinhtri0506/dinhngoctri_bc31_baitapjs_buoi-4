document
  .getElementById("btn-definedTriangleType")
  .addEventListener("click", definedTriangleType);

function definedTriangleType() {
  var edgeAValue = document.getElementById("edge-a").value * 1;
  var edgeBValue = document.getElementById("edge-b").value * 1;
  var edgeCValue = document.getElementById("edge-c").value * 1;

  var result = document.getElementById("output-result-type-of-triangle");

  // Xác định tam giác vuông
  // Tạo biến với tam giác vuông (có cạnh huyền là a,...)
  var rightTriangleWithHypotenuseA =
    Math.pow(edgeAValue, 2) ==
    Math.pow(edgeBValue, 2) + Math.pow(edgeCValue, 2);
  var rightTriangleWithHypotenuseB =
    Math.pow(edgeBValue, 2) ==
    Math.pow(edgeAValue, 2) + Math.pow(edgeCValue, 2);
  var rightTriangleWithHypotenuseC =
    Math.pow(edgeCValue, 2) ==
    Math.pow(edgeAValue, 2) + Math.pow(edgeBValue, 2);

  // Kiểm tra giá trị người dùng nhập vào có phù hợp (độ dài >= 0)
  // Nếu không phù hợp, trả thông báo và dừng lệnh
  if (edgeAValue <= 0 || edgeBValue <= 0 || edgeCValue <= 0) {
    result.innerHTML = "Độ dài các cạnh phải lớn hơn 0";
    result.style.color = "red";
  }
  // Ngược lại, tiếp tục xác định loại tam giác
  else {
    result.style.color = "black";
    // Nếu các biến có tam vuông với cạnh huyền là a, hoặc b, hoặc c là đúng
    if (
      rightTriangleWithHypotenuseA == true ||
      rightTriangleWithHypotenuseB == true ||
      rightTriangleWithHypotenuseC == true
    ) {
      // Kiểm tra tiếp xem có phải là tam giác vuông cân hay không
      if (
        edgeAValue == edgeBValue ||
        edgeAValue == edgeCValue ||
        edgeBValue == edgeCValue
      ) {
        result.innerHTML = "Đây là tam giác vuông cân";
      } else {
        // Nếu không, trả về kết quả là tam giác vuông
        result.innerHTML = "Đây là tam giác vuông";
      }
    }
    // Kiểm tra xem có hai cạnh bất kì nào bằng nhau hay không
    else if (
      edgeAValue == edgeBValue ||
      edgeAValue == edgeCValue ||
      edgeBValue == edgeCValue
    ) {
      // Nếu có kiểm tra tiếp xem cạnh a có đồng thời bằng cạnh b và c hai không (nếu có, chương trình xác định đây là tam giác đều)
      if (edgeAValue == edgeBValue && edgeAValue == edgeCValue) {
        result.innerHTML = "Đây là tam giác đều";
      }
      // Hoặc trả về đây là tam giác cân
      else {
        result.innerHTML = "Đây là tam giác cân";
      }
      // Nếu không có các trường hợp nêu trên, xác định đây là tam giác thường
    } else {
      result.innerHTML = "Đây là tam giác thường";
    }
  }

  console.log(
    { rightTriangleWithHypotenuseA },
    { rightTriangleWithHypotenuseB },
    { rightTriangleWithHypotenuseC }
  );
  console.log(
    Math.pow(edgeAValue, 2),
    Math.pow(edgeBValue, 2),
    Math.pow(edgeCValue, 2)
  );
}
